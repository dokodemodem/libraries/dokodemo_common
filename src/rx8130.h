/*
 * RTC Driver
 * rx8130.h
 *
 // Original Code by JeeLabs http://news.jeelabs.org/code/
 // Released to the public domain! Enjoy!
 // https://www.elecrow.com/wiki/index.php?title=File:RTC.zip
 *
 * Modifyed by CircuitDesign,Inc.
 */

#ifndef _RX8130_H_
#define _RX8130_H_

#include <Board.h>

enum class RtcTimerPeriod : uint8_t
{
    MSECONDS = 1,
    SECONDS = 2,
    MINITES = 3,
    HOURS = 4
};

enum class SleepMode : uint8_t
{
    IDLE0 = 0,
    IDLE1,
    IDLE2,
    DEEPSLEEP
};

class RX8130
{
public:
    RX8130();
    bool begin(uint8_t addr = RX8130_I2C_ADD, TwoWire &theWire = Wire);
    DateTime now();
    void adjust(const DateTime &dt);
    bool _valid = false;
    void setTimer(const RtcTimerPeriod period, const uint16_t count);
    void stopTimer(void);
    bool chkBattery(void);
    bool chkTimer(void);
    uint32_t readRAM(void);
    void writeRAM(uint32_t val);
    void setAlarm(uint8_t mode,uint8_t hour,uint8_t min);
    void stopAlarm(void);
    void clearAlarm(void);

    bool _vddDown = false;

private:
    void writebyte(uint8_t add, uint8_t data);
    uint8_t readbyte(uint8_t add);
    TwoWire *_wire;
    uint8_t i2c_addr;
};
#endif