/*
 * RTC Driver
 * rx8130.cpp
 // Original Code by JeeLabs http://news.jeelabs.org/code/
 // Released to the public domain! Enjoy!
 // https://www.elecrow.com/wiki/index.php?title=File:RTC.zip
 *
 * Modifyed by CircuitDesign,Inc.
 */
#include "rx8130.h"

////////////////////////////////////////////////////////////////////////////////
// utility code, some of this could be exposed in the DateTime API if needed

const uint8_t daysInMonth[] PROGMEM = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; // has to be const or compiler compaints

// number of days since 2000/01/01, valid for 2001..2099
static uint16_t date2days(uint16_t y, uint8_t m, uint8_t d)
{
    if (y >= 2000)
        y -= 2000;
    uint16_t days = d;
    for (uint8_t i = 1; i < m; ++i)
        days += pgm_read_byte(daysInMonth + i - 1);
    if (m > 2 && y % 4 == 0)
        ++days;
    return days + 365 * y + (y + 3) / 4 - 1;
}

////////////////////////////////////////////////////////////////////////////////
// DateTime implementation - ignores time zones and DST changes
// NOTE: also ignores leap seconds, see http://en.wikipedia.org/wiki/Leap_second

DateTime::DateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec)
{
    if (year >= 2000)
        year -= 2000;
    y = year;
    m = month;
    d = day;
    hh = hour;
    mm = min;
    ss = sec;
}

uint8_t DateTime::dayOfWeek() const
{
    uint16_t day = date2days(y, m, d);
    return (day + 6) % 7; // Jan 1, 2000 is a Saturday, i.e. returns 6
}

////////////////////////////////////////////////////////////////////////////////
// RTC_RX8130 implementation

static uint8_t bcd2bin(uint8_t bcd)
{
    int i0, i1;
    i0 = bcd & 0x0F;
    i1 = (bcd >> 4) * 10;
    return i0 + i1;
}
static uint8_t bin2bcd(uint8_t i)
{
    byte n0, n1;
    n0 = i % 10;
    n1 = (i / 10) % 10;
    return (n1 << 4) | n0;
}

RX8130::RX8130(void)
{
}

void RX8130::writebyte(uint8_t add, uint8_t data)
{
    _wire->beginTransmission(i2c_addr);
    _wire->write(add);
    _wire->write(data);
    _wire->endTransmission();
}

uint8_t RX8130::readbyte(uint8_t add)
{
    _wire->beginTransmission(i2c_addr);
    _wire->write(add);
    _wire->endTransmission();

    uint8_t num_bytes = 1;
    _wire->requestFrom(i2c_addr, num_bytes);
    return _wire->read();
}

bool RX8130::begin(uint8_t addr, TwoWire &theWire)
{
    i2c_addr = addr;
    _wire = &theWire;
    uint8_t flag;

    flag = readbyte(0x1d);

    // check VDD down or not
    if (flag & 0x04)//check RSF bit
    {
        _vddDown = true;
    }
    else
    {
        _vddDown = false;
    }
    writebyte(0x1d, flag & 0xfb); // RSF clear
    // writebyte(0x1d, flag & 0x7b); // VBLF,RSF clear

    //発生したときに試すこと。
    //ソフトウェアリセット
    //1CのFSEL1,FSEL0を００にしてみる。
    if (flag & 0x2) // check VLF
    {
        _valid = false;

#if 1
        // software reset
        writebyte(0x1e, 0);
        writebyte(0x1e, 0x80);
        writebyte(0x50, 0x6c);
        writebyte(0x53, 0x01);
        writebyte(0x66, 0x03);
        writebyte(0x6b, 0x02);
        writebyte(0x6b, 0x01);
        delay(125);
#endif

        do{
            writebyte(0x1d, 0); // VLF clear
            delay(10);
            flag = readbyte(0x1d); // Flag

        } while (flag & 0x2);

        writebyte(0x30, 0); // DTE clear

        _wire->beginTransmission(i2c_addr);
        _wire->write(0x1c);
        _wire->write(0xc4); // 0x1c
        _wire->write(0x00); // 0x1d
        _wire->write(0x00); // 0x1e
        //_wire->write(0x30); // 0x1f 2次電池
        _wire->write(0x10); // 0x1f 1次電池
        
        _wire->endTransmission();

        DateTime init = DateTime(2024, 1, 1, 0, 0, 0);
        adjust(init);
    }
    else
    {
        _valid = true;
    }
#if 1
    //writebyte(0x1f, 0x32);// 0x1f 2次電池
    writebyte(0x1f, 0x10);// 0x1f 1次電池
#else
    writebyte(0x1f, 0x03);
    //writebyte(0x1f, 0x13);
    writebyte(0x1f, 0x33);
    //writebyte(0x1f, 0x23);
#endif
    writebyte(0x30, 0x00);

    return true;
}

void RX8130::adjust(const DateTime &dt)
{
    uint8_t data[7];
    data[0] = bin2bcd(dt.second());
    data[1] = bin2bcd(dt.minute());
    data[2] = bin2bcd(dt.hour());
    data[3] = bin2bcd(dt.dayOfWeek());
    data[4] = bin2bcd(dt.day());
    data[5] = bin2bcd(dt.month());
    data[6] = bin2bcd(dt.year() - 2000);

    uint8_t ctl = readbyte(0x1e);
    ctl |= 0x40;
    writebyte(0x1e, ctl); // Set Stop

    _wire->beginTransmission(i2c_addr);
    _wire->write(0x10);
    _wire->write(data[0]);
    _wire->write(data[1]);
    _wire->write(data[2]);
    _wire->write(data[3]);
    _wire->write(data[4]);
    _wire->write(data[5]);
    _wire->write(data[6]);
    _wire->endTransmission();

    ctl = ctl & ~0x40;
    writebyte(0x1e, ctl); // Clear Stop
}

DateTime RX8130::now()
{
    uint8_t data[7];

    _wire->beginTransmission(i2c_addr);
    _wire->write(0x10);
    _wire->endTransmission();

    uint8_t num_bytes = 7;
    _wire->requestFrom(i2c_addr, num_bytes);
    data[0] = _wire->read();
    data[1] = _wire->read();
    data[2] = _wire->read();
    data[3] = _wire->read();
    data[4] = _wire->read();
    data[5] = _wire->read();
    data[6] = _wire->read();

    uint8_t ss = bcd2bin(data[0] & 0x7F);
    uint8_t mm = bcd2bin(data[1]);
    uint8_t hh = bcd2bin(data[2]);
    // day of month data[3]
    uint8_t d = bcd2bin(data[4]);
    uint8_t m = bcd2bin(data[5]);
    uint16_t y = bcd2bin(data[6]) + 2000;

    return DateTime(y, m, d, hh, mm, ss);
}

void RX8130::setTimer(RtcTimerPeriod period, uint16_t count)
{
    uint8_t val = readbyte(0x1c);
    writebyte(0x1c, (val & 0xe8) | (uint8_t)period); // Clear TE bit,set period

    val = readbyte(0x1d);
    writebyte(0x1d, val & 0xc7); // Clear TF bit

    val = readbyte(0x1e);
    writebyte(0x1e, (val & 0xfe) | 0x10); // Set TIE,Clear TBKE

    if (period == RtcTimerPeriod::MSECONDS)
    {
        count = (uint16_t)((float)count / 15.625);
    }

    // set count
    _wire->beginTransmission(i2c_addr);
    _wire->write(0x1a);
    _wire->write(count & 0xff);        // 0x1a
    _wire->write((count >> 8) & 0xff); // 0x1b
    _wire->endTransmission();

    val = readbyte(0x1c);
    writebyte(0x1c, val | 0x10); // Set TE bit
}

void RX8130::stopTimer(void)
{
    uint8_t val = readbyte(0x1e);
    writebyte(0x1e, (val & 0x6f)); // Clear TEST,TIE bit

    val = readbyte(0x1c);
    writebyte(0x1c, (val & 0xef)); // Clear TE bit
}

bool RX8130::chkBattery(void)
{
    uint8_t val = readbyte(0x1d);
    return val & 0x1;
}

bool RX8130::chkTimer(void)
{
    uint8_t val = readbyte(0x1c); // check TE bit
    return val & 0x10 ? true : false;
}

uint32_t RX8130::readRAM(void)
{
    _wire->beginTransmission(i2c_addr);
    _wire->write(0x20);
    _wire->endTransmission();

    uint8_t data[4];
    uint8_t num_bytes = 4;
    _wire->requestFrom(i2c_addr, num_bytes);
    data[0] = _wire->read();
    data[1] = _wire->read();
    data[2] = _wire->read();
    data[3] = _wire->read();

    uint32_t ret = ((uint32_t)data[3] << 24) | ((uint32_t)data[2] << 16) | ((uint32_t)data[1] << 8) | ((uint32_t)data[0]);

    return ret;
}

void RX8130::writeRAM(uint32_t val)
{
    _wire->beginTransmission(i2c_addr);
    _wire->write(0x20);
    _wire->write(val & 0xff);
    _wire->write((val >> 8) & 0xff);
    _wire->write((val >> 16) & 0xff);
    _wire->write((val >> 24) & 0xff);
    _wire->endTransmission();
}

void RX8130::setAlarm(uint8_t mode, uint8_t hour, uint8_t min)
{
    stopAlarm();

    if (mode == 0)
    {
        // min alerm
        uint8_t settimeM = bin2bcd(min);
        writebyte(0x17, settimeM);
        writebyte(0x18, 0x80);
        writebyte(0x19, 0x80);
    }
    else
    {
        // hour alerm
        uint8_t settimeM = bin2bcd(min);
        uint8_t settimeH = bin2bcd(hour);
        writebyte(0x17, settimeM);
        writebyte(0x18, settimeH);
        writebyte(0x19, 0x80);
    }

    // uint8_t wada = readbyte(0x1c);
    // writebyte(0x1c, (wada | 0x08)); // Set wada bit

    clearAlarm();

    uint8_t val = readbyte(0x1e);
    writebyte(0x1e, (val | 0x08)); // Set AIE bit
}

void RX8130::stopAlarm(void)
{
    uint8_t val = readbyte(0x1e);
    writebyte(0x1e, (val & 0x77)); // stop interrupt TEST, AIEbit

    clearAlarm();
}

void RX8130::clearAlarm(void)
{
    uint8_t val = readbyte(0x1d);
    writebyte(0x1d, val & 0xf7); // Clear AF bit
}
